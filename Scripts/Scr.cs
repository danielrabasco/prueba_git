﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scr : MonoBehaviour {

    GameObject cube;
    private int handCounter;
    private bool r;
    private bool l;
    private bool rFirst;

    // Use this for initialization
    void Start()
    {
        cube = this.gameObject;
        handCounter = 0;
    }

    private void OnTriggerEnter(Collider other)
    {       
        r = (other.tag == "RightHand");
        l = (other.tag == "LeftHand");
        if ((r || l) && handCounter == 0 /*&& cube.GetComponent<Rigidbody>().mass <1*/) {
            if (r)
            {
                transform.SetParent(other.transform);
                handCounter++;
                rFirst = true;
            }
            else { // Lo coge con la izquierda primero, siendo esta la maestra. Y teniendo que "congelar" posteriormente la derecha.
                transform.SetParent(other.transform);
                handCounter++;
                rFirst = false;
            }
        }
        /*else if ((r && l) && handCounter == 1)
        {
            // if handCounter!=0 que la otra mano se quede "quieta" cogiendo el objeto
            handCounter++;
            if (rFirst)
            {
                //bloquear mano izquierda
            }
            else {
                //bloquear mano derecha
            }
        }*/
    }
    /*private void OnTriggerExit(Collider other)
    {
        
    }*/
}
