﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    GameObject player;
    public GameObject cubeRight;
    public GameObject cubeLeft;
     
	// Use this for initialization
	void Start () {
        player = gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            SendMessage("changeRightTarget", cubeRight);
            SendMessage("changeLeftTarget", cubeLeft);
        }
	}
}
