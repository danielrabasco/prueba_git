﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour {

    public OptitrackRigidBody rh;
    public OptitrackRigidBody lh;
    public IKControl jesse;

    private int handCounter;
    private bool r;
    private bool l;
    private GameObject derecha;
    private GameObject izquierda;
    private bool inHand;
    // Use this for initialization
    void Start () {
        handCounter = 0;
        derecha = GameObject.FindGameObjectWithTag("RightHand");
        izquierda = GameObject.FindGameObjectWithTag("LeftHand");
        inHand = false;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private void OnTriggerEnterCustom(Collider other)
    {
        r = (other.tag == "RightHand");
        l = (other.tag == "LeftHand");
        if ((r || l) /*&& handCounter == 0 && cube.GetComponent<Rigidbody>().mass <1*/)
        {
            if (r)
            {
                handCounter++;
                if (gameObject.GetComponent<Rigidbody>().mass < 1)
                {
                    //transform.parent = null;
                    transform.SetParent(derecha.transform);
                }
                else
                {
                    if (handCounter == 2)
                    {
                        gameObject.GetComponent<Rigidbody>().isKinematic = true;
                        gameObject.GetComponent<Rigidbody>().useGravity = false;
                        transform.SetParent(izquierda.transform);
                        //rh.usePostionTracking = false;
                    }
                }
            }
            else
            { // Lo coge con la izquierda primero, siendo esta la maestra. Y teniendo que "congelar" posteriormente la derecha.
                handCounter++;
                if (gameObject.GetComponent<Rigidbody>().mass < 1)
                {
                    //transform.parent = null;
                    transform.SetParent(derecha.transform, false);
                }
                else
                {
                    if (handCounter == 2)
                    {
                        gameObject.GetComponent<Rigidbody>().isKinematic = true;
                        gameObject.GetComponent<Rigidbody>().useGravity = false;
                        transform.SetParent(derecha.transform);                      
                        //lh.usePostionTracking = false;
                    }
                }
            }
        }
    }

    private void OnTriggerExitCustom(Collider other)
    {
        r = (other.tag == "RightHand");
        l = (other.tag == "LeftHand");
        if (r || l)
        {
            handCounter--;
           /* if (handCounter == 1 && gameObject.GetComponent<Rigidbody>(). mass >= 1) {
                transform.parent = null;
                gameObject.GetComponent<Rigidbody>().useGravity = true;
                gameObject.GetComponent<Rigidbody>().isKinematic = false;
            }*/
        }
    }
}
