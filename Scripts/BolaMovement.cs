﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaMovement : MonoBehaviour {

    private bool enMano;
    public GameObject mano;
    public GameObject posicion;
    private Rigidbody rb;
    private Vector3 positionPrev;
    public double speed;
	// Use this for initialization
	void Start () {
        enMano = false;
        rb = GetComponent<Rigidbody>();
        positionPrev = transform.position;
            }
	
	// Update is called once per frame
	void Update () {
        if (enMano)
        {
            print(rb.velocity);
            //transform.position = posicion.transform.position;
            Renderer rend = GetComponent<Renderer>();
            rend.material.color = Color.red;
            if ((transform.position - positionPrev).magnitude > speed)
            {
                enMano = false;
                rb.transform.parent = null;
                rb.AddForce((transform.position - positionPrev) * 40, ForceMode.Impulse);
                //rb.AddForce(-rb.transform.forward, ForceMode.Impulse);
                rb.isKinematic = false;
            }
        }
        positionPrev = transform.position;
	}


    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject == mano)
        {
            enMano = true;
            rb.transform.SetParent(mano.transform);
        }
    }
}
